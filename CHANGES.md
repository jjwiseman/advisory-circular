# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [unreleased]

### Added

- Added `update.sh` script.


## [1.0.0] - 2021-02-06

### Added

- Now tries to detect obviously incompatible mictronics.de JSON.

### Changed

- Updated `install.sh` to use a compatible mictronics.de JSON database.
