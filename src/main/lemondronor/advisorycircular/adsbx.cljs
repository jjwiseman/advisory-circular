;; Scripts a real live chromium browser to take screenshots from ADS-B
;; Exchange.

(ns lemondronor.advisorycircular.adsbx
  (:require
   ["puppeteer" :as puppeteer]
   [clojure.string :as string]
   [goog.string :as gstring]
   [kitchen-async.promise :as p]
   [lemondronor.advisorycircular.logging :as logging]
   [lemondronor.advisorycircular.util :as util]
   goog.string.format))

(declare logger log-debug log-verbose log-info log-warn log-error)
(logging/deflog "adsbx" logger)

(def user-agent "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/53 (KHTML, like Gecko) Chrome/15.0.87")


;; (defn wait-for-property [page prop-string]
;;   (let [pieces (string/split prop-string ".")]
;;     (p/loop [pieces-checking (take 1 pieces)
;;              pieces-remaining (drop 1 pieces)
;;              ]
;;       (println "Waiting for" (string/join "." pieces-checking))
;;       (p/resolve (.waitForFunction page (string/join "." pieces-checking)))
;;       (when (seq pieces-remaining)
;;         (recur (concat pieces-checking (take 1 pieces-remaining))
;;                (drop 1 pieces-remaining))))))


(defn current-time-secs []
  (/ (.getTime (js/Date.)) 1000))


(defn delete-node [page selector]
  (p/try
    (.evaluate
     page
     (fn [selector]
       (let [node (.querySelector js/document selector)]
         (.removeChild (.-parentNode node) node)))
     selector)
    (p/catch :default e
      (log-error "No nodes matching selector %s" selector))))


(defn persistently-delete-nodes [page selectors]
  (.evaluate
   page
   (fn [selectors]
     (js/setInterval
      (fn []
        (doseq [selector selectors]
          (try
            (let [node (.querySelector js/document selector)]
              (when node
                (.removeChild (.-parentNode node) node)))
            (catch js/Error e))))
      100))))


(defn tar1090-url [icao options]
  (let [url (gstring/format
             "https://tar1090.adsbexchange.com/?icao=%s&zoom=%s&noIsolation&hideButtons&hideSidebar"
             icao
             (or (:zoom options) 14))]
    (if (:simple-tracks options)
      (str url "&monochromeMarkers=ffff55&monochromeTracks=010101&outlineColor=505050")
      url)))


(defn screenshot-aircraft
  ([icao lat lon]
   (screenshot-aircraft icao lat lon {}))
  ([icao lat lon options]
   (log-info "%s Taking screenshot of aircraft at %s %s" icao lat lon)
   (let [icao-selector (gstring/format "//td[contains(., '%s')]"
                                       (string/upper-case icao))
         launch-options {:headless (get options :headless? true)
                         :defaultViewport (get options :viewport {:width 1600
                                                                  :height 800})}
         took-screenshot?_ (atom false)]
     (p/race
      [(p/promise [resolve reject]
                  (js/setTimeout
                   #(when-not @took-screenshot?_
                      (log-error "Screenshot timing out")
                      (reject (js/Error. "Timeout")))
                   (get options :timeout 30000)))
       (p/let [browser (puppeteer/launch (clj->js launch-options))
               page (.newPage browser)]
         (if-let [user-agent (:user-agent options)]
           (.setUserAgent page user-agent))
         (log-debug "Navigating")
         (let [url (tar1090-url icao options)]
           (log-info "%s Getting screenshot at url %s with zoom %s" icao url (or (:zoom options) 14))
           (p/all [(.waitForNavigation page)
                   (.goto page
                          url
                          (clj->js {:referer "https://adsbexchange.com/"}))]))
         (util/timeout 100)
         (when-let [layer (:layer options)]
           (.evaluate page (str "ol.control.LayerSwitcher.forEachRecursive(layers_group, l => { if (l.get('name') == '"
                                layer
                                "') { console.log(l.get('name')); ol.control.LayerSwitcher.setVisible_(OLMap, l, true); } });")))
         (util/timeout 2900)
         (persistently-delete-nodes page ["#adsense" ".adsbygoogle" "[Title=\"Close\"]" ".FIOnDemandWrapper"])
         (p/all [(.type (.-keyboard page) "l")
                 (util/timeout 2000)])
         (let [path (get options :output-path (str "screenshot-"
                                                   (.toFixed (current-time-secs) 0)
                                                   "-"
                                                   icao
                                                   ".png"))]
           (log-verbose "Writing %s" path)
           (p/do
             (.screenshot page (clj->js {:path path
                                         :clip (get options :clip {:width 1200
                                                                   :height 800
                                                                   :x 200
                                                                   :y 0})}))
             (log-debug "screenshot done")
             (reset! took-screenshot?_ true)
             (log-debug "closing browser")
             (.close browser)
             (log-debug "closing browser done")
             path)))]))))


;; (defn main [& args]
;;   (let [settings (fs/readFileSync "vrs-settings.json" "utf-8")
;;         zoom 13
;;         icao (nth args 0)
;;         lat (js/parseFloat (nth args 1))
;;         lon (js/parseFloat (nth args 2))]
;;     (screenshot-aircraft icao lat lon {:zoom zoom :vrs-settings settings})))
